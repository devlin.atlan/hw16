#include <iostream>
#include <ctime>
using namespace std;

//��� ����� �������
class Vector
{
private:
	//��� ������� ����������, ������������� �����, ������ � ������ ������� �� ����� 0,0,0
	double x = 0, y = 0, z = 0;
public:
	//������������� ����� ���������
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	//������� ������ �� ����� ���� ����������
	void showVector()
	{
		cout << "x = " << x;
		cout << ";      y = " << y;
		cout << ";      z = " << z << "\n";
	}
	//������� �������� ������ �������
	double vectorModule()
	{
		return sqrt(x * x + y * y + z * z);
	}
};

int main()
{
	// �������� ������� 16

	cout << "Homework 16\n\n";
	//����� � ������ �������
	const int N=6;
	int arr[N][N];
	
	//����������� �������� �����
	time_t now = time(0);
	tm *curTime = localtime(&now);

	//����������, ���������� �� ����� ������ ��� ������ � ������� � �� � �����
	int printRow = curTime->tm_mday % N;
	int printRowSum = 0;

	//���������� �������
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			arr[i][j] = i + j;
		}
	}
	
	//����� ������� � ����������� ���������� �������� ������ ��� ������ � �������
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << arr[i][j] << "  ";
			if (i == printRow) { printRowSum += arr[i][j]; }
		}
		cout << "\n";
	}

	cout <<"\n" << printRowSum << "\n\n\n";


	// �������� ������� 17
	cout << "Homework 17\n\n";
	//�������� ������� �� ����� ������, ������� ���������� ����������� ��������� ��������
	Vector myVector = Vector(3.5, 1.7, 4.786);
	
	//����� ������� ������ ���������� ������� � �������
	myVector.showVector();

	//����� � ������� ������ �������
	cout << myVector.vectorModule() << "\n";

	system("pause");
	return 0;
}